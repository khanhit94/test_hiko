<?php

use Faker\Generator as Faker;
use \App\Models\Product;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'SKU' => $faker->text,
        'price' => $faker->randomFloat(null, 1, 10),
        'description' => $faker->text,
    ];
});