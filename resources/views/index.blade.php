<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link href="https://fonts.googleapis.com/css?family=Cormorant+Unicase" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Cormorant+Unicase|Eater" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Anton|Cormorant+Unicase" rel="stylesheet">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>

<h1 class="text-center text-uppercase display-4 font-weight-bold" style="background-color: #74B3CE; color: white; font-family: 'Cormorant Unicase', serif;"> Product Manager</h1>
<div class="container">
	<div class="d-flex flex-row justify-content-end ">
		<button type="button" class="btn btn-warning text-white" data-toggle="modal" data-target="#myModal">
		 Add Product
		</button>
	</div>

	<div >
		<h2 style="color: #062f4f; font-family: 'Cormorant Unicase', serif;" class="font-weight-bold mb-4"> All Products </h2>
		<div id="records_content">
			
		</div>
	</div>
	<nav aria-label="Page navigation example">
			<div id="pagination">
			
			</div>
	</nav>
</div>
<!-- The Modal -->
<div class="modal fade" id="myModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Modal Heading</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
			
				<div class="form-group">
					<label>  SKU: </label>
					<input type="text" name="sku" id="sku" placeholder="SKU" class="form-control">
				</div>

				<div class="form-group">
					<label> Name </label>
					<input type="text" name="name" id="name" placeholder="Name" class="form-control">
				</div>

				<div class="form-group">
					<label> Description. </label>
					<input type="text" name="description" id="description" placeholder="Description" class="form-control">
				</div>

				<div class="form-group">
					<label> Price. </label>
					<input type="text" name="price" id="price" placeholder="Price" class="form-control">
				</div>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal" onclick="addRecord()" id=>Save</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!--  after update -->
<div class="modal fade" id="update_product_modal">
	<div class="modal-dialog">
		<div class="modal-content">

		<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Modal Heading</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<div class="form-group">
				<label> SKU </label>
				<input type="text" name="sku" id="update_sku" placeholder="SKU" class="form-control">
			</div>

			<div class="form-group">
				<label> Name </label>
				<input type="text" name="name" id="update_name" placeholder="Name" class="form-control">
			</div>

			<div class="form-group">
				<label> Description </label>
				<input type="text" name="description" id="update_description" placeholder="Description" class="form-control">
			</div>

			<div class="form-group">
				<label> Price </label>
				<input type="text" name="price" id="update_price" placeholder="Price" class="form-control">
			</div>
		</div>

		<!-- Modal footer -->
		<div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-primary" onclick="UpdateProductDetails()" >Update</button>
			<input type="hidden" id="hidden_user_id">
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script>
	
$(document).ready(function () {
    readRecords(); 
	$.ajaxSetup({async:false});
});
	
function addRecord() {	
	var name =  $("#name").val();
	var sku =  $("#sku").val();
	var description = $("#description").val();
	var price =  $("#price").val();
	var product = {
		name:name,
		SKU:sku,
		description:description,
		price:price,
	};

	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:"api/v1/products",
		type:'POST',
		cache: false,
		processData: false,
		async: false,
		data: JSON.stringify(product),
		contentType: "application/json",
		dataType: 'json',
			async: false,
		success:function(data, status){
			readRecords();
		},
		error:function(data, status){
			showError(JSON.parse(data.responseText).errors);
		},
	});
}

//Display Records
function readRecords(page){		
	var readrecords = "readrecords";
	$.ajax({
		url:"api/v1/products?page=" + page,
		type:"GET",
		success:function(data,status){
			console.log(data);
			var html = renderRecoard(data.data);
			var pagination = renderPagination(data.meta.pagination);
			$('#records_content').html(html);
			$('#pagination').html(pagination);
		},
	});
}
 function renderRecoard(data) {
	var html = `<table class="table table-bordered table-striped ">
				<tr class="bg-dark text-white">
					<th>No.</th>
					<th>SKU</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th> 
					<th>Edit Action</th>
					<th>Delete Action</th>
				</tr>`;
			
	$.each(data , function (index, value) {
		var attributes = value.attributes;
		html += `<tr>  
					<td>${index + 1}</td>
					<td>${attributes.SKU}</td>
					<td>${attributes.name}</td>
					<td>${attributes.description}</td>
					<td>${attributes.price}</td>
					<td>
						<button onclick="GetProductDetails(${value.id})" class="btn btn-success">Edit</button>
					</td>
					<td>
						<button onclick="DeleteProduct(${value.id})" class="btn btn-danger">Delete</button>
					</td>
				</tr>`;
	});
	html += '</table>';
	return html;
}

//delete product
function DeleteProduct(id) {
	var conf = confirm("are you sure ?");
	if(conf == true) {
		$.ajax({
			url:"api/v1/products/" + id,
			type:'DELETE',
			success:function(data, status){
				readRecords();
			}
		});
	}
}

//get detail product
function GetProductDetails(id) {
	$("#hidden_user_id").val(id)
	  $.ajax({
		url:"api/v1/products/" + id,
		type:"GET",
		success:function(data, status){
			console.log(data.data);
			var attributes = data.data.attributes;
			$("#update_sku").val(attributes.SKU);
            $("#update_name").val(attributes.name);
            $("#update_description").val(attributes.description);
            $("#update_price").val(attributes.price);
		},
	});
    $("#update_product_modal").modal("show");
}

//update product
function UpdateProductDetails() {
    var sku = $("#update_sku").val();
    var name = $("#update_name").val();
    var description = $("#update_description").val();
    var price = $("#update_price").val();
    var hidden_user_id = $("#hidden_user_id").val();
	var product = {
		name:name,
		SKU:sku,
		description:description,
		price:price,
	};
    $.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:"api/v1/products/" + hidden_user_id,
		type:'PATCH',
		cache: false,
		processData: false,
		async: false,
		data: JSON.stringify(product),
		contentType: "application/json",
		dataType: 'json',
			async: false,
		success:function(data, status){
			readRecords();
			$("#update_product_modal").modal("hide");
		},
		error:function(data, status){
			showError(JSON.parse(data.responseText).errors);
		},
	});
}

//show error
function showError(data) {
	console.log(data);
	var error = '';
	$.each(data, function (index, value) {
		error += value.detail + '\n';
	});
	alert(error);
}

function renderPagination(data) {
	var html = '<ul class="pagination">';
	if (data.current_page > 1) {
		html += `<li class="page-item"><a class="page-link" onclick="readRecords(${(data.current_page) - 1})">Previous</a></li>`;
	}
	for (var index = 1; index <= data.total_pages; index++) {
		html += `<li class="page-item"><a class="page-link" onclick="readRecords(${index})">${index}</a></li>`;	
	}
	if (data.current_page < data.total_pages) {
		html += `<li class="page-item"><a class="page-link" onclick="readRecords(${(data.current_page) + 1})">Next</a></li>`;
	}
	html += '</ul>';
	return html;
}
</script>

</body>
</html>