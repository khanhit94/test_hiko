<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductCreateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $res = $this->postJson('/api/v1/products', [], []);
        $res->assertStatus(422);
    }

    public function testInvalidSku()
    {
        $res = $this->postJson('/api/v1/products', 
            [
                'name' => 'name',
                'description' => 'description',
                'price' => 1000
            ]
        );
        $res->assertStatus(422);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'SKU',
                    ],
                ],
            ],
        ]);
    }

    public function testCreateWithoutName()
    {
        $res = $this->postJson('/api/v1/products', 
            [
                'SKU' => 'sku',
                'description' => 'description',
                'price' => 1000
            ]
        );
        $res->assertStatus(422);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'name',
                    ],
                ],
            ],
        ]);
    }

    public function testInvaldPrice()
    {
        $res = $this->postJson('/api/v1/products', 
            [
                'name' => 'name',
                'SKU' => 'sku',
                'description' => 'description',
                'price' => 'price'
            ]
        );
        $res->assertStatus(422);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'price',
                    ],
                ],
            ],
        ]);
    }

    public function testSkuUnique()
    {
        $product = factory(\App\Models\Product::class)->create();
        $res = $this->postJson('/api/v1/products', 
            [
                'name' => 'name',
                'description' => 'description',
                'price' => 1000,
                'SKU' =>  $product->SKU
            ]
        );
        $res->assertStatus(422);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'SKU',
                    ],
                ],
            ],
        ]);
    }

    public function testCreateSuccess()
    {
        $data =  [
            'name' => 'name',
            'description' => 'description',
            'price' => 1000,
            'SKU' =>  'SKU'
        ];
        $res = $this->postJson('/api/v1/products', $data);
        $res->assertStatus(201);
        $this->assertDatabaseHas('products', $data);
    }
}
