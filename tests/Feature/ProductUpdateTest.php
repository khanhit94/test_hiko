<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductUpdateTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testInvalidateRequest()
    {
        $res = $this->patchJson('/api/v1/products', [], []);
        $res->assertStatus(405);
    }

    public function testNotfoundModel()
    {
        $res = $this->patchJson('/api/v1/products/1', [], []);
        $res->assertStatus(404);
    }

    public function testSkuUnique()
    {
        $product = factory(\App\Models\Product::class)->create();
        $product2 = factory(\App\Models\Product::class)->create();
        $res = $this->patchJson('/api/v1/products/'. $product->id, 
            [
                'SKU' =>  $product2->SKU
            ]
        );
        $res->assertStatus(422);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'SKU',
                    ],
                ],
            ],
        ]);
    }

    public function testInvaldPrice()
    {
        $product = factory(\App\Models\Product::class)->create();
        $res = $this->patchJson('/api/v1/products/'. $product->id, 
            [
                'price' =>  'price'
            ]
        );
        $res->assertStatus(422);
        $res->assertJson([
            'errors' => [
                [
                    'title' => 'Validation Error',
                    'source' => [
                        'pointer' => 'price',
                    ],
                ],
            ],
        ]);
    }

    public function testUpdateSuccess()
    {
        $product = factory(\App\Models\Product::class)->create();
        $data =  [
            'name' => 'name111',
            'description' => 'description update',
            'price' => 1000,
            'SKU' =>  'SKUupdate'
        ];
        $res = $this->patchJson('/api/v1/products/'. $product->id, $data);
        $res->assertStatus(200);
        $this->assertDatabaseHas('products', $data);
    }
}
