<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductDeleteTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotfoundModel()
    {
        $res = $this->deleteJson('/api/v1/products/1', [], []);
        $res->assertStatus(404);
    }

    public function testDeleteSuccess()
    {
        $product = factory(\App\Models\Product::class)->create();
        $product2 = factory(\App\Models\Product::class)->create();
        $res = $this->deleteJson('/api/v1/products/'. $product->id);
        $res->assertStatus(204);
        $this->assertDatabaseMissing('products', ['id' => $product->id]);
    }
}
