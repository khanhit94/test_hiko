<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductDetailTest extends TestCase
{
    public function tearDown()
    {
        \Mockery::close();
        parent::tearDown();
    }

    public function testNotfoundModel()
    {
        $res = $this->getJson('/api/v1/products/1', [], []);
        $res->assertStatus(404);
    }

    public function testDetailSuccess()
    {
        $product = factory(\App\Models\Product::class)->create();
       
        $res = $this->getJson('/api/v1/products/'. $product->id);
        $res->assertStatus(200);
    }
}
