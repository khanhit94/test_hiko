<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\ProductRepository;
use App\Models\Product;
use App\Validators\ProductValidator;

/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    public function presenter()
    {
        return \App\Presenters\ProductPresenter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

     /**
     * Override method create to save product
     * @param  array  $attributes attributes from request
     * @return object
     */
    public function create(array $attributes)
    {
        $product = parent::create($attributes);

        return $product;
    }

    public function update(array $attributes, $id)
    {
        $product = parent::update($attributes, $id);

        return $product;
    }
    
}
