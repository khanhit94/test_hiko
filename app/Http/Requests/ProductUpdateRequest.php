<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'SKU' => [
                'sometimes',
                'required',
                Rule::unique('products')->ignore($this->id),
                'alpha_num',
                'max:12'
            ],
            'name' => 'sometimes|required|alpha_num|max:50',
            'description' => 'string|sometimes|max:500',
            'price' => 'sometimes|numeric|min:0',
        ];
    }
}
